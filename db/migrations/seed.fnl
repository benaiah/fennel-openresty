(local create-users-table (require :migrations.create-users-table))
(local create-seed-users (require :migrations.create-seed-users))
(local create-messages-table (require :migrations.create-messages-table))

(fn seed []
  (create-users-table)
  (create-seed-users)
  (create-messages-table))

(seed)

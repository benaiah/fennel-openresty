(local pgmoon (require :pgmoon))
(local dbconfig (require :config))
(local fennelview (require :modules.fennel.fennelview))

(fn create-users-table []
  (local pg (pgmoon.new dbconfig))
  (local pg-status (assert (: pg :connect)))

  (local users-table-created (assert (: pg :query "
create extension if not exists pgcrypto;
create table if not exists users (
  id uuid not null default gen_random_uuid() primary key,
  email text not null unique,
  password text not null,
  confirmed boolean default false
);
")))

  (print (.. "creating users table: " (fennelview users-table-created)))

  users-table-created)

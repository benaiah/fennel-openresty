(local pgmoon (require :pgmoon))
(local dbconfig (require :config))
(local fennelview (require :modules.fennel.fennelview))

(local create-users-table (require :migrations.create-users-table))

(fn create-test-users []
  (when (create-users-table)
    (local pg (pgmoon.new dbconfig))
    (local pg-status (assert (: pg :connect)))
    (local test-user-created
           (assert (pg:query "
insert into users (email, password, confirmed) values (
  'test@example.com',
  crypt('test-password', gen_salt('bf')),
  true
) on conflict do nothing;
")))
    (print (.. "creating test user: " (fennelview test-user-created)))
    test-user-created))

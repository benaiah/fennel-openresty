(local pgmoon (require :pgmoon))
(local dbconfig (require :config))
(local fennelview (require :modules.fennel.fennelview))

(fn create-messages-table []
  (local pg (pgmoon.new dbconfig))
  (local pg-status (assert (pg:connect)))

  (local messages-created (pg:query "
create table if not exists messages (
  id bigserial primary key,
  message text,
  response text,
  timestamp timestamp with time zone default current_timestamp
);
"))

  (print (.. "creating messages table - success?: " (fennelview messages-created)))
  messages-created)

(local jwt (require :resty.jwt))
(local jwt-secret (os.getenv :FENNEL_OPENRESTY_JWT_SECRET))

(fn get-token [{:id id :email email :confirmed confirmed}]
  (: jwt :sign jwt-secret
     {:header {:typ :JWT :alg :HS512}
      :payload {:sub id
                :email email
                :confirmed confirmed
                :iat (ngx.time)}}))

(fn verify-token [token]
  (let [jwt-obj (: jwt :load_jwt token)]
    (: jwt :verify_jwt_obj jwt-secret jwt-obj)))

(fn verify-token-and-retrieve-payload [token]
  (let [jwt-obj (: jwt :load_jwt token)
        verified? (: jwt :verify_jwt_obj jwt-secret jwt-obj)]
    (if verified? jwt-obj.payload false)))

{:get-token get-token
 :verify-token verify-token
 :verify-token-and-retrieve-payload verify-token-and-retrieve-payload}

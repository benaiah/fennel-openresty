(global ngx ngx)
(local pgmoon (require :pgmoon))
(local cjson (require :cjson))
(local html (fn [...] (.. "<!doctype html>"
                          ((require :server.html) ...))))
(local fennel (require :modules.fennel.fennel))
(local fennelview (require :modules.fennel.fennelview))
(local lume (require :modules.lume.lume))
(local jwt (require :server.jwt))

(local merge lume.merge)

(local pg (pgmoon.new (require :db.config)))
(local pg-status (assert (pg:connect)))

(local (headers headers-err) (ngx.req.get_headers))
(when headers-err (error headers-err))

(local method (ngx.req.get_method))
(local args (ngx.req.get_uri_args))

(fn serialize-args [args]
  (let [strings []]
    (var strings-length 0)
    (each [k v (pairs args)]
      (if (= (type v) :string)
          (do
            (set strings-length (+ strings-length 1))
            (tset strings strings-length (.. (ngx.escape_uri k) "=" (ngx.escape_uri v))))

          (= (type v) :table)
          (let [escaped-k (ngx.escape_uri k)]
            (each [i vv (ipairs v)]
              (set strings-length (+ strings-length 1))
              (tset strings strings-length (.. escaped-k "=" (ngx.escape_uri vv)))))

          (error "bad args")))
    (if (> strings-length 0) (table.concat strings :&) "")))

(fn say-serialized [tab]
  (if (and headers (= headers.Accept "text/x-fennelview"))
      (do (set ngx.header.Content-Type "text/x-fennelview")
          (ngx.say (fennelview tab {:indent ""})))
      (do (set ngx.header.Content-Type "application/json")
          (ngx.say (cjson.encode tab)))))

;; middleware
(fn jwt-payload-key [] nil)
(fn user-key [] nil)
(fn with-verified-bearer-jwt [route-fn]
  (fn [context]
    (let [verified-payload (get-verified-jwt-payload)]
      (if verified-payload
          (route-fn
           (merge (if context context {})
                  {jwt-payload-key verified-payload
                   user-key {:id verified-payload.sub
                             :email verified-payload.email
                             :confirmed verified-payload.confirmed}}))
          (do (set ngx.status ngx.HTTP_UNAUTHORIZED)
              (say-serialized
               {:error {:message "Not authorized" :code 401}}))))))
(fn with-confirmed-user [route-fn]
  (-> (fn [context]
        (let [{:confirmed confirmed} (. context user-key)]
          (if confirmed (route-fn context)
              (do (set ngx.status ngx.HTTP_UNAUTHORIZED)
                  (say-serialized
                   {:error {:message "User not confirmed"
                            :code 401}})))))
      with-verified-bearer-jwt))


(fn index-route []
  (say-serialized {:authenticate :/api/authenticate
                   :verify :/api/verify
                   :user :/api/user
                   :confirm-signup :/api/confirm-signup}))

(fn unknown-route []
  (let [message (.. "Error 404 - unknown route " ngx.var.uri
                    " for method " method)
        response {:error {:message message :code 404}}]
    (set ngx.status ngx.HTTP_NOT_FOUND)
    (say-serialized response)))

;; returns a user id if successful
(fn authenticate-user-by-email [{:email email :password password}]
  (ngx.log ngx.NOTICE (.. "email: " email " pw: " password))
  (let [esc-email (pg:escape_literal email)
        esc-password (pg:escape_literal password)
        query (.. "select id, email, confirmed from users where"
                     " email=lower(" esc-email ") and"
                     " password=crypt(" esc-password ", password)"
                     " limit 1;")
        result (pg:query query)]
    (and result (. result 1))))

(local authenticate-wrong-method-error-message
       (.. "Method not allowed - use a POST with a JSON body"
           " including 'email' and 'password' to log in and receive a"
           " JWT which can be used as a bearer token to authenticate"
           "  with the rest of the API."))

(fn authenticate-route []
  (match method
    :POST
    (do (ngx.req.read_body)
        (let [raw-body (ngx.req.get_body_data)
              body (if raw-body (cjson.decode raw-body) {})]
          (if (and body.email body.password)
              (let [user (authenticate-user-by-email body)]
                (if user
                    (say-serialized {:token (jwt.get-token user)})
                    (do (set ngx.status ngx.HTTP_UNAUTHORIZED)
                        (say-serialized
                         {:error {:message "Unauthorized" :code 401}}))))
              (do (set ngx.status ngx.HTTP_BAD_REQUEST)
                  (say-serialized
                   {:error {:message "Bad request" :code 400}})))))
    _ (do (set ngx.status ngx.HTTP_NOT_ALLOWED)
          (say-serialized
           {:error {:message authenticate-wrong-method-error-message
                    :code 405}}))))

(fn get-verified-jwt-payload []
  (let [[typ token] (lume.split headers.Authorization " ")]
    (and typ token (= (string.lower typ) "bearer")
         (jwt.verify-token-and-retrieve-payload token))))

(fn verify-route []
  (let [verified-payload (get-verified-jwt-payload)]
    ;; the "not not" construct forces a boolean for serialization
    (say-serialized {:verified (not (not verified-payload))})))

(local
 user-route
 (-> (fn [context]
       (let [{:id id} (. context user-key)
             query (when id
                     (.. "select email from users where id="
                         (pg:escape_literal id) " limit 1;"))
             result (when query
                      (pg:query query))
             email (when (and result (. result 1))
                     (. result 1 :email))]
         (if email (say-serialized {:id id :email email})
             (do (set ngx.status ngx.HTTP_NOT_FOUND)
                 (say-serialized {:error {:message "User not found"
                                          :code 404}})))))
     with-verified-bearer-jwt))

(fn signup-route [context]
  (match method
    :POST nil))

(fn path-key [] nil)
(fn confirm-signup-route [context]
  (let [path (. context path-key)]
    (match path
      [nil] ;; missing confirmation token error
      (do (set ngx.status ngx.HTTP_BAD_REQUEST)
          (say-serialized {:error {:message "Token missing"
                                   :code 400}}))
      [token nil] nil ;; look up token and confirm user
      _ (do (set ngx.status ngx.HTTP_BAD_REQUEST)
            (say-serialized {:error {:message "Bad URL"
                                     :code 400}})))))

(fn test-httpbin-route []
  (let [http (require :resty.http)
        httpc (http.new)
        args-string (serialize-args args)
        url (.. "http://httpbin.org/get?" args-string)
        _ (ngx.log ngx.NOTICE (.. "url: " url " args-string: " args-string))
        (res err) (httpc:request_uri url {:method :GET
                                          :keepalive_timeout 60000
                                          :keepalive_pool 10})]
    (if (not res)
        (ngx.say "failed to request: " err)
        (do
          (set ngx.status res.status)
          (each [k v (pairs res.headers)] (tset ngx.header k v))
          (ngx.say res.body)))))

(fn echo-args-route []
  (say-serialized args))

(fn get-route [path]
  (match path
    [:api nil] index-route
    [:api :authenticate nil] authenticate-route
    [:api :verify nil] verify-route
    [:api :user nil] user-route
    [:api :confirm-signup] confirm-signup-route
    [:api :test-httpbin nil] test-httpbin-route
    [:api :echo-args nil] echo-args-route
    _ unknown-route))

(fn string-ends-with [str ending]
  (or (= ending "") (= (string.sub str (* -1 (# ending))) ending)))

(fn router [uri]
   (let [trimmed-uri (if (string-ends-with uri "/") (string.sub uri 1 -2) uri)
         path (lume.slice (lume.split trimmed-uri :/) 2)
         route (get-route path)
         context {path-key (lume.slice path 3)}]
     (route context)))

(router ngx.var.uri)

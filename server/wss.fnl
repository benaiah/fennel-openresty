(global ngx ngx)
(local server (require :resty.websocket.server))
(local pgmoon (require :pgmoon))
(local cjson (require :cjson))
(local jwt (require :server.jwt))
(local fennelview (require :modules.fennel.fennelview))

(local pg (pgmoon.new (require :db.config)))
(local (pg-status pg-err) (: pg :connect))
((fn []
   (when (not pg-status)
     (ngx.log ngx.ERR
              (.. "error connecting to db: " (tostring pg-err))))))

(fn log-message-to-db [message response]
  (ngx.log ngx.INFO (fennelview [message response]))
  (when pg-status
    (ngx.log ngx.ERR (fennelview [message response]))
    (let [query
          (.. "insert into messages (message, response) values ("
              (: pg :escape_literal message) ","
              (: pg :escape_literal response) ");")
          (result err) (: pg :query query)]
      (when (not result)
        (ngx.log ngx.ERR (.. "error logging message: " (tostring err)
                             " / " (tostring result)))))))

(local fennel (require :modules.fennel.fennel))
(local fennelview (require :modules.fennel.fennelview))

(fn eval-message [message]
  (let [(ok result) (xpcall fennel.eval fennel.traceback message)]
    (if ok (fennelview result) result)))

(fn wss [] ;; must be wrapped in a function to avoid a top-level
           ;; vararg
  (local (wb err)
         (: server :new {:timeout 10000 :max_payload_len 65535}))
  (var continue true)
  (var session-token nil)
  (when (not wb)
    (ngx.log ngx.ERR "failed to connect to new websocket: " err)
    (ngx.exit 444)
    (set continue false))
  (while continue
    (local (data typ err) (: wb :recv_frame))
    (if (and (not data) (not (string.find err :timeout 1 true)))
        (do
          (ngx.log ngx.ERR "failed to receive a frame: " err)
          (set continue false))
        
        (= typ :close)
        (let [(bytes err) (: wb :send_close 1000 "close frame")]
          (if (not bytes)
              (ngx.log ngx.ERR "failed to send the close frame: " err)
              (ngx.log ngx.INFO
                       "closing with status code " err
                       " and message " data))
          (set continue false))

        (= typ :ping)
        (let [(bytes err) (: wb :send_pong data)]
          (when (not bytes)
            (ngx.log ngx.ERR "failed to send frame: " err)
            (set continue false)))
        
        (= typ :pong) nil
        
        (= typ :text)
        (let [message (cjson.decode data)
              (bytes err)
              (match message
                {:action :auth :token nil}
                (: wb :send_text
                   (cjson.encode
                    {:re :auth
                     :action :error
                     :message "Missing token!"}))
                
                {:action :auth :token token}
                (let [payload
                      (jwt.verify-token-and-retrieve-payload token)]
                  (ngx.log ngx.ERR (fennelview payload))
                  (if
                    (not payload)
                    (do (set ngx.status ngx.HTTP_UNAUTHORIZED)
                        (cjson.encode
                         {:re :auth
                          :action :error
                          :message "Invalid token!"}))
                    (not payload.confirmed)
                    (do (set ngx.status ngx.HTTP_UNAUTHORIZED)
                        (cjson.encode
                         {:re :auth
                          :action :error
                          :message "User not confirmed!"}))
                    ;; else
                    (do (set session-token token)
                        (: wb :send_text
                           (cjson.encode
                            {:re :auth
                             :action :success
                             :message
                             "Authenticated successfully."})))))
                
                {:action :compile :src nil}
                (: wb :send_text
                   (cjson.encode
                    {:re :compile
                     :action :error
                     :message "Cannot compile without src!"}))

                {:action :compile :src src}
                (: wb :send_text
                   (if session-token
                       (cjson.encode
                        {:re :compile
                         :action :success
                         :value
                         (let [(_ result) 
                               (pcall fennel.compileString src)]
                           (log-message-to-db src result)
                           result)})
                       (cjson.encode
                        {:re :compile
                         :action :error
                         :message "Not authenticated!"})))

                _ (: wb :send_text
                     (cjson.encode
                      {:action :error
                       :message "Message not understood!"})))]
          (when (not bytes)
            (ngx.log ngx.ERR "failed to send text: " err)
            (ngx.exit 444)
            (set continue false)))

        (not (string.find err :timeout 1 true))
        (let [(bytes err) (: wb :send_text
                             (.. "unknown frame type: " (tostring typ)
                                 " data: " (tostring data)
                                 " err: " (tostring err)))]
          (when (not bytes) 
            (ngx.log ngx.ERR "failed to send a text frame: " err)
            (ngx.exit 444)
            (set continue false)))))

  (let [(bytes err) (: wb :send_close 1000 "close frame")]
    (when (not bytes)
      (ngx.log ngx.ERR "failed to send the close frame: " err))))

(wss)

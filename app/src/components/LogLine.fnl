(require-macros :src.react-macros)
(local React (require "react"))

(local bubble-style
       (js! {:position :relative
             :backgroundColor :#555555
             :borderRadius :.4em
             :marginBottom :1em
             :padding "1em 1em"
             :whiteSpace :pre}))

(local bubble-inner-style
       (js! {:overflowX :auto
             :width :100%
             :height :100%}))

(local after-left-bubble-style
       (js! {:position :absolute
             :left 0
             :top :50%
             :width 0
             :height 0
             :borderTop "0.625em solid transparent"
             :borderRight "0.625em solid #555555"
             :borderLeft 0
             :borderBottom 0
             :marginTop :-0.312em
             :marginLeft :-0.625em}))

(local after-right-bubble-style
       (js! {:position :absolute
             :right 0
             :top :50%
             :width 0
             :height 0
             :borderTop "0.625em solid transparent"
             :borderLeft "0.625em solid #555555"
             :borderRight 0
             :borderBottom 0
             :marginTop :-0.312em
             :marginRight :-0.625em}))

(local info-style
       (js! {:fontSize :75%
             :textAlign :center
             :color :#DDDDDD
             :marginBottom :1em}))

(component! LogLine [{:children message :message-type message-type}]
  (c! :div {}
      (if (or (not message-type) (= message-type :info))
          (c! :div {:style info-style} message)

          (or (= message-type :send) (= message-type :reply))
          (c! [:div {:style bubble-style}
               [:div {:style bubble-inner-style} message]
               [:div {:style
                      (if (= message-type :reply)
                          after-right-bubble-style
                          after-left-bubble-style)}]]))))

LogLine

(require-macros :src.react-macros)
(local React (require :react))
(local {:use-state use-state
        :use-ref use-ref
        :use-effect use-effect} (require "../react-helpers.lua"))

(component! WebSocketDebugForm
  [{:connect connect :disconnect disconnect :on-message on-message}]
  (let [input-element (use-ref nil)
        (current-message set-current-message) (use-state "")]
    (use-effect
     (fn []
       (when input-element.current (: input-element.current :focus))
       nil))
    (c! [:form {:onSubmit
                (fn [_ e]
                  (: e :preventDefault)
                  (on-message current-message)
                  (set-current-message ""))}
         [:div {}
          [:button {:type :button :onClick connect} :Connect]
          [:button {:type :button :onClick disconnect} :Disconnect]]
         [:div {}
          [:textarea {:style {:width :100%
                              :minWidth :100%
                              :height :50px}
                      :value current-message
                      :ref input-element
                      :onChange
                      (fn [] (set-current-message
                              input-element.current.value))}]]
         [:div {:className :buttons}
          [:button {:type :submit} :Send]]])))

WebSocketDebugForm

(require-macros :src.react-macros)
(local React (require :react))
(local {:use-context use-context} (require "../react-helpers.lua"))
(local DispatchContext (require "./DispatchContext.lua"))

(local header-style
       (js! {:width :100%
             :backgroundColor :#222222
             :color :#EEEEEE
             :paddingLeft :1em
             :paddingRight :1em
             :paddingTop :1ex
             :paddingBottom :1ex
             :marginBottom :2ex
             :height :37px
             :display :flex
             :flexFlow "row nowrap"
             :justifyContent :space-between
             :alignItems :center}))

(component! AppHeader [{:user user}]
  (let [dispatch (use-context DispatchContext)]
    (c! [:div {:style header-style}
         [:span {}
          (if (= user.from-cache? nil) ""
              user.loading? "Logging in..."
              user.token (if user.email user.email "<unknown email>")
              "Logged out")]
         (when user.token
             (c! [:a {:href "#"
                      :onClick (fn [] (dispatch {:type :logout}))}
                  :Logout]))])))

AppHeader

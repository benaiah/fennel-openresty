(require-macros :src.react-macros)
(local React (require "react"))
(local {:use-effect use-effect
        :use-ref use-ref}
       (require "../react-helpers.lua"))
(local LogLine (require "./LogLine.lua"))

;; scrolls to the last child of container-ref.current when deps change
(fn use-scroll-to-last-child [deps]
  (let [container-ref (use-ref nil)]
    (use-effect
     (fn []
       (when (and container-ref.current
                  (not (= container-ref.current js.null))
                  container-ref.current.lastChild
                  (not (= container-ref.current.lastChild js.null)))
         (: container-ref.current.lastChild
            :scrollIntoView (js! {}))))
     deps)
    container-ref))

(component! ScrollingContainer [{:children children}]
  (let [container-ref (use-scroll-to-last-child
                       (js! [(or (and children children.length) 0)]))]
    (c! :div {:style {:overflowY :auto
                      :width :100%
                      :height "calc(100vh - 200px)"
                      :marginBottom 0
                      :marginTop :1rem
                      :padding "0 1rem"
                      :paddingBottom :1rem}
              :ref container-ref}
        children)))

ScrollingContainer

(require-macros :src.react-macros)
(local React (require "react"))
(local jwtDecode (require "jwt-decode"))
(local jwt-decode (fn [token] (jwtDecode nil token)))
(local {:then then
        :fetch-json fetch-json} (require "../http-helpers.lua"))
(local {:use-state use-state
        :use-context use-context} (require "../react-helpers.lua"))
(local DispatchContext (require "./DispatchContext.lua"))

(local console {:log (fn [...] (: js.global.console :log ...))
                :error (fn [...] (: js.global.console :error ...))})

(local JSON js.global.JSON)

(fn perform-login [dispatch {:email email :password password}]
  (dispatch {:type :login-begin})
  (-> (fetch-json
       "/api/authenticate"
       (js! {:method :POST
             :body (: JSON :stringify
                      (js! {:email email :password password}))}))
      (then (fn [{:token token}]
              (let [{:email email :sub id :confirmed confirmed}
                    (jwt-decode token)]
                {:token token :email email
                 :id id :confirmed confirmed})))
      (then (fn [user]
              (dispatch {:type :login-complete :user user}))
            (fn [err]
              (dispatch {:type :login-failed :err err})))))

(component! Input
  [{:type input-type :label label :onChange onChange :value value}]
  (local input
         (c! :input {:type input-type
                     :value value
                     :onChange onChange
                     :style {:width :auto}}))
  (c! :div {}
      (if label
          (c! [:label {:style {:display :block}}
               [:span {:style {:width :100px
                               :display :inline-block}} label " "]
               input])
          input)))

(component! LoginForm [{:loading? loading?}]
  (let [dispatch (use-context DispatchContext)
        (current-email set-current-email) (use-state "")
        (current-password set-current-password) (use-state "")
        (logging-in? set-logging-in) (use-state false)]
    (if loading?
        (c! [:div {:style {:marginBottom :50px}} "Logging in..."])
        (c! [:form
             {:className :login
              :style {:marginBottom :50px}
              :onSubmit
              (fn [_ e]
                (when (not loading?)
                  (perform-login
                   dispatch {:email current-email
                             :password current-password})))}
             [:p {} "Please log in: "]
             [Input {:label :Email:
                     :type :email
                     :value current-email
                     :onChange (fn [_ {:target {:value v}}]
                                    (set-current-email v))}]
             [Input {:label :Password:
                     :type :password
                     :value current-password
                     :onChange (fn [_ {:target {:value v}}]
                                    (set-current-password v))}]
             [:button {:type :submit} :Login]]))))

LoginForm

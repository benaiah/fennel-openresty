(require-macros :src.react-macros)

(local React (require "react"))

(fn use-state [initial]
  (let [val (React.useState nil initial)
        state (. val 0)
        set-state (. val 1)]
    (values state (fn [new-state] (set-state nil new-state)))))

(fn use-reducer [reducer initial]
  (let [val (React.useReducer
             nil (fn [_ ...] (reducer ...)) initial)
        state (. val 0)
        dispatch (fn [...] ((. val 1) nil ...))]
    (values state dispatch)))

(fn use-ref [initial] (React.useRef nil initial))

(fn create-context [initial] (React.createContext nil initial))
(fn use-context [context] (React.useContext nil context))

(fn use-effect [effect dependencies]
  (React.useEffect nil effect dependencies))

(fn use-layout-effect [effect dependencies]
  (React.useLayoutEffect nil effect dependencies))

(fn use-previous [value]
  (let [ref (use-ref)]
    (use-effect (fn [] (set ref.current value)))
    ref.current))

(fn table-to-array [tab]
  (local ret (js! []))
  (each [_ el (ipairs tab)]
    (: ret :push el))
  ret)

(fn get-children-as-array [maybe-children]
  (if (: js.global.Array :isArray maybe-children) maybe-children
      (= (type maybe-children) :table) (table-to-array maybe-children)
      (js! [maybe-children])))

{:use-state use-state
 :use-reducer use-reducer
 :use-effect use-effect
 :use-layout-effect use-layout-effect
 :use-ref use-ref
 :use-previous use-previous
 :create-context create-context
 :use-context use-context
 :get-children-as-array get-children-as-array}

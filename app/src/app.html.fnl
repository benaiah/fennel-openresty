(fn map [f tbl]
  (let [out {}]
    (each [i v (ipairs tbl)]
      (tset out i (f v)))
    out))

(fn map-kv  [f tbl]
  (let [out {}]
    (each [k v (pairs tbl)]
      (table.insert out (f k v)))
    out))

(fn to-attr [k v]
  (if (= v true) k
      (.. k "=\"" v"\"")))

(fn tag [tag-name attrs]
  (assert (= (type attrs) "table") "Missing attrs table")
  (let [attr-str (table.concat (map-kv to-attr attrs) " ")]
    (.. "<" tag-name " " attr-str">")))

(fn html [doctype]
  (if (= (type doctype) "string")
      doctype
      (let [[tag-name attrs & body] doctype]
        (.. (tag tag-name attrs)
            (table.concat (map html body) " ")
            "</" tag-name ">"))))

(local ret
       (.. "<!doctype html>\n"
           (html [:html {}
                  [:head {}
                   [:meta {:charset "UTF-8"}]]
                  [:style {}
                   (.. "body {margin:0;}"
                       " html {box-sizing: border-box;}"
                       " *, *:before, *:after {box-sizing: inherit;}")]
                  [:body {:style "margin:0;"}
                   [:div {:id :react-root}]
                   [:script {:src "app.js"}]]])))

(print ret)

ret

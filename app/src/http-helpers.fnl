(local Promise js.global.Promise)

(fn then [p f c] (: p :then (fn [_ a] (f a))
                    (when c (fn [_ a] (c a)))))

;; assumes all responses, even errors, contain a json body. rejects
;; http errors.
(fn fetch [url options] (: js.global :fetch url options))

(fn get-json-fetcher [fetch-fn]
  (fn [...]
    (then (fetch ...)
          (fn [res]
            (then (: res :json)
                  (fn [json]
                    (if res.ok json
                        (: Promise :reject
                           {:res res :err json}))))))))
(local fetch-json (get-json-fetcher fetch))

{:then then
 :fetch fetch
 :get-json-fetcher get-json-fetcher
 :fetch-json fetch-json}

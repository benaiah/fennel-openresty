(require-macros :src.react-macros)
(local React (require :react))
(local ReactDOM (require :react-dom))
(local lume (require "./lib/lume.lua"))
(local merge lume.merge)
(local fennelview (require "./lib/fennelview.lua"))
(fn fennelview-oneline [thing options]
  (let [result
        (string.gsub
         (fennelview thing (merge (or options {}) {:indent ""}))
         "\n" " ")]
    result))
(local ljson (require "./lib/json.lua"))
(local <> React.Fragment)
(local {:use-state use-state
        :use-reducer use-reducer
        :use-effect use-effect
        :use-ref use-ref
        :create-context create-context}
       (require "./react-helpers.lua"))
(local DispatchContext (require "./components/DispatchContext.lua"))
(local AppHeader (require "./components/AppHeader.lua"))
(local LoginForm (require "./components/LoginForm.lua"))
(local WebSocketDebugForm
       (require "./components/WebSocketDebugForm.lua"))
(local ScrollingContainer
       (require "./components/ScrollingContainer.lua"))
(local LogLine (require "./components/LogLine.lua"))

(local console {:log (fn [...] (: js.global.console :log ...))
                :error (fn [...] (: js.global.console :error ...))
                :group
                (fn [...] (: js.global.console :groupCollapsed ...))
                :group-end
                (fn [...] (: js.global.console :groupEnd ...))})

(local Promise js.global.Promise)
(local JSON js.global.JSON)

(fn map-to-js [seq fun]
  (local ret (js! []))
  (each [i el (pairs seq)]
    (let [key (if (= (type i) :number) (- i 1) i)]
      (tset ret key (fun el i))))
  ret)

(local initial-app-state
       {:user {:loading? nil
               :email nil
               :id nil
               :token nil
               :from-cache? nil}
        :ws {:connected? nil :authenticated? nil}
        :log-messages []})

(fn first-line-of [str] (. (lume.split str "\n") 1))
(fn get-action-preview [a]
  (let [{:type t} a]
    (match t
      :login-complete (.. t ": " (first-line-of a.user.email))
      :log-message (.. t ": " (first-line-of a.message))
      nil "<unknown action>"
      _ t)))

;; can return an optional second value, which indicates how to merge
;; the new state. this defaults to :merge which performs a shallow
;; merge with the current state. it can be set to :replace to replace
;; the entire state instead.
(local concat lume.concat)
(fn get-action-patch [s a]
  (match a.type
    :login-begin
    {:user {:loading? true}}
    :login-complete
    {:user (merge s.user a.user
                  {:loading? false :from-cache? false})}
    :login-failed
    {:user {:err a.err}}
    :logout
    (values (merge initial-app-state {:user {:clear-cache true}})
            :replace)
    
    :user-restored
    {:user (merge s.user a.user {:from-cache? true})}
    :user-unrestorable
    {:user (merge s.user {:from-cache? false})}
    :user-cache-cleared
    {:user {:clear-cache false :from-cache? false}}

    :ws-connecting
    {:ws {:connection a.ws :connecting? true}}
    :ws-connected
    {:ws (merge s.ws {:connected? true :connecting? false})}
    :ws-disconnected
    {:ws {}}
    :ws-authenticated
    {:ws (merge s.ws {:authenticated? true})}

    :log-message
    {:log-messages (concat s.log-messages
                           [{:message a.message
                             :message-type a.message-type}])}

    _ {}))

(fn app-reducer [state action]
  (console.group (get-action-preview action))
  (print (.. "action: " (fennelview action)))
  (let [(state-patch state-action) (get-action-patch state action)
        state-action (or state-action :merge)
        new-state
        (if (= state-action :merge) (merge state state-patch)
            (= state-action :replace) state-patch
            state)]
    (console.group "old state")
    (print (fennelview state))
    (console.group-end)
    (console.group "new state")
    (print (fennelview new-state))
    (console.group-end)
    (console.group-end)
    (set js.global.fennel_openresty_state new-state)
    new-state))

(fn log-with [dispatch message-type message]
  (let [m-type (or message-type :info)]
    (if (and message-type (not message))
        (dispatch {:type :log-message
                   :message message-type
                   :message-type :info})

        (and message-type message)
        (dispatch {:type :log-message
                   :message message
                   :message-type message-type})
        
        dispatch
        (fn [a-message a-message-type]
          (log-with dispatch a-message a-message-type))
        

        log-with)))

(fn create-websocket
  [{:url url
    :on-open onopen
    :on-close onclose
    :on-error onerror
    :on-message onmessage}]
  (let [ws (js.new js.global.WebSocket url)]
    (set ws.onopen onopen)
    (set ws.onerror onerror)
    (set ws.onmessage onmessage)
    (set ws.onclose onclose)
    ws))

(fn ws-when-open [state-ref dispatch e]
  (let [state state-ref.current]
    (when state
      (log-with dispatch :info "connected")
      (dispatch {:type :ws-connected})
      (when state.user.token
        (state.ws.connection:send
         (: JSON :stringify
            (js! {:action :auth :token state.user.token})))))))

(fn ws-when-error [_ dispatch event]
  (log-with dispatch :info "websocket error!")
  (dispatch {:type :ws-disconnected}))

(fn ws-when-message [_ dispatch event]
  (let [decoded-message (ljson.decode event.data)
        log (log-with dispatch)]
    (match decoded-message
      nil (log :info "received message without valid JSON!")

      {:re :auth :action :success}
      (dispatch {:type :ws-authenticated})

      {:re :compile :action :success :value value}
      (log :reply value)

      _ (log :info (.."unrecognized message: " event.data)))))

(fn ws-when-close [_ dispatch event]
  (log-with dispatch :info (.. "disconnected: " (or event.reason "")))
  (dispatch {:type :ws-disconnected}))

(local ws-url "ws://localhost:8080/wss")
(fn ws-connect [state dispatch callbacks]
  (let [log (log-with dispatch)]
    (if state.ws.connected? (log :info "already connected")
        state.ws.connecting? (log :info "already connecting")
        (dispatch {:type :ws-connecting
                   :ws (create-websocket
                        {:url ws-url
                         :on-open callbacks.open
                         :on-close callbacks.close
                         :on-error callbacks.error
                         :on-message callbacks.message})}))))

(fn ws-disconnect [state dispatch]
  (let [log (log-with dispatch)]
    (if (not (or state.ws.connected? state.ws.connecting?))
        (log :info "already disconnected")
        (do (dispatch {:type :ws-disconnected})
            (: state.ws.connection :close)))))

(fn ws-send-message [state dispatch message]
  (let [log (log-with dispatch)]
    (if (not state.ws.connected?) (log :info "not connected")
        (let [json-message
              (ljson.encode {:action :compile :src message})]
          (log :send message)
          (: state.ws.connection :send json-message)))))

(fn ws-get-callbacks [state-ref dispatch]
  {:open (fn [_ e] (ws-when-open state-ref dispatch e))
   :error (fn [_ e] (ws-when-error state-ref dispatch e))
   :message (fn [_ e] (ws-when-message state-ref dispatch e))
   :close (fn [_ e] (ws-when-close state-ref dispatch e))})

(local container-style
       (js! {:backgroundColor :#444
             :color :#eee
             :minHeight :100vh
             :fontFamily :sans-serif}))

(local inner-container-style
       (js! {:paddingLeft :1em
             :paddingRight :1em}))

(fn use-cached-user [state dispatch]
  (use-effect
   (fn []
     (local ls js.global.localStorage)
     (if (not state.user.token)
         (let [cached-user (: ls :getItem :fennel-openresty-user)
               parsed-user
               (when (and cached-user
                          (not (= cached-user js.null)))
                 (ljson.decode cached-user))]
           (if state.user.clear-cache
               (do (: ls :removeItem :fennel-openresty-user)
                   (dispatch {:type :user-cache-cleared}))
               parsed-user
               (dispatch {:type :user-restored :user parsed-user})
               (dispatch {:type :user-unrestorable})))
         (: ls :setItem :fennel-openresty-user
            (ljson.encode state.user))))
   (js! [(or state.user.token js.null)])))

(fn render-messages [messages]
  (c! ScrollingContainer {}
      (map-to-js
       messages
       (fn [msg i]
         (c! LogLine
             {:key i :message-type msg.message-type}
             msg.message)))))

(component! App []
  (let [(state dispatch) (use-reducer app-reducer initial-app-state)
        state-ref (use-ref nil)
        _ (set state-ref.current state)
        log (fn [...] (log-with dispatch ...))
        ws-callbacks (ws-get-callbacks state-ref dispatch)
        connect (fn [] (ws-connect state dispatch ws-callbacks))
        disconnect (fn [] (ws-disconnect state dispatch))
        send-message (fn [m] (ws-send-message state dispatch m))]
    (use-cached-user state dispatch)
    (c! [DispatchContext.Provider {:value dispatch}
         [:div {:style container-style}
          [AppHeader {:user state.user}]
          [:div {:style inner-container-style}
           (if (= state.user.from-cache? nil) (c! <> {})
               state.user.token
               (c! [<> {}
                    [:div {}
                     (if state.ws.connected?
                         "WebSocket connected"
                         "WebSocket not connected")]
                    [WebSocketDebugForm
                     {:connect connect
                      :disconnect disconnect
                      :on-message send-message}]
                    (render-messages state.log-messages)])
               (c! LoginForm {:loading? state.user.loading?}))]]])))

(: ReactDOM :render
   (c! App {})
   (: js.global.document :getElementById :react-root))

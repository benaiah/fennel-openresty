module.exports = {
  entry: './src/index.js',
  output: {
    filename: 'app.js'
  },
  module: {
    rules: [
      {
	test: /\.lua$/,
        use: [
          { loader: "fengari-loader" }
        ]
      },
    ]
  }
}

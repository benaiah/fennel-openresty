.PHONY: all           # Builds the server, the frontend app, and the DB scripts
all: server app db

.PHONY: clean         # Cleans the server and the webapp, but not the DB data
clean: clean-server clean-app

.PHONY: fennel
fennel:
	$(MAKE) -C modules/fennel fennel

.PHONY: server        # Builds the server
server: fennel
	$(MAKE) -C server all

.PHONY: clean-server  # Cleans the server
clean-server:
	$(MAKE) -C server clean

.PHONY: app           # Builds the frontend app
app: fennel
	$(MAKE) -C app all

.PHONY: clean-app     # Cleans the frontend app
clean-app:
	$(MAKE) -C app clean

.PHONY: db            # Builds the DB scripts
db: fennel
	$(MAKE) -C db all

.PHONY: clean-db      # Cleans the DB scripts and development DB data
clean-db:
	$(MAKE) -C db clean

.PHONY: up            # Start the application in development
up:
	./bin/fail-in-container && docker-compose up -d

.PHONY: down          # Shut down the application in development
down:
	./bin/fail-in-container && docker-compose down

.PHONY: enter         # Enters the web container which contains the necessary tooling to build the project
enter:
	./bin/fail-in-container && docker-compose exec web bash

.PHONY: container
container:
	./bin/fail-in-container && docker-compose build web

.PHONY: log-errors    # View nginx error log
log-errors:
	less +F nginx/logs/error.log

.PHONY: log-access    # View nginx access log
log-access:
	less +F nginx/logs/access.log

.PHONY: seed          # Seed the DB with test data
seed:
	$(MAKE) -C db seed

.PHONY: targets       # Shows available targets
targets:
	echo "\nAvailable targets: "; grep -E '^.PHONY: ' makefile | sed 's/^.PHONY: /  /'

.PHONY: help          # Alias of "targets"
help: targets

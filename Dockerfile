FROM openresty/openresty:stretch

LABEL maintainer="Benaiah Mischenko <benaiah@mischenko.com>"

WORKDIR /app

COPY . /app

EXPOSE 80

# Setup nginx config
RUN echo "include /app/nginx/conf/nginx.conf;" > /etc/nginx/nginx.conf \
    && echo "include /app/nginx/conf/nginx.conf;" > /usr/local/openresty/nginx/conf/nginx.conf
    
# Install tooling from apt, including OpenResty Package Manager
RUN DEBIAN_FRONTEND=noninteractive apt-get update \
    && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
       apt-utils git make openresty-opm rlwrap wget

# Add fennel to PATH
RUN printf '#!/usr/bin/env bash\nexec rlwrap /app/modules/fennel/fennel "$@"\n' | tee /usr/local/bin/fennel \
    && chmod +x /usr/local/bin/fennel

# Install node
ENV NVM_DIR /usr/local/nvm
ENV NODE_VERSION 10.15.1
RUN mkdir $NVM_DIR \
    && curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.34.0/install.sh | bash \
    && . $NVM_DIR/nvm.sh \
    && nvm install $NODE_VERSION \
    && nvm alias default $NODE_VERSION \
    && nvm use default
ENV NODE_PATH $NVM_DIR/v$NODE_VERSION/lib/node_modules
ENV PATH $NVM_DIR/versions/node/v$NODE_VERSION/bin:$PATH

# Install yarn (node package installer, npm alternative)
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | DEBIAN_FRONTEND=noninteractive apt-key add - \
    && echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list \
    && DEBIAN_FRONTEND=noninteractive apt-get remove -y cmdtest \
    && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends apt-transport-https \
    && DEBIAN_FRONTEND=noninteractive apt-get update \
    && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends yarn

# Install opm packages
RUN opm get leafo/pgmoon && opm get SkyLothar/lua-resty-jwt

# OpenResty doesn't have any program named "lua" by default, but the
# fennel script uses that as its shebang. LuaJit works just fine to
# run fennel in the build process.
RUN ln -sf /usr/local/openresty/luajit/bin/luajit /usr/local/bin/lua \
    && make

# Setup nginx logs
# RUN ln -sf /app/nginx/logs/access.log /dev/stdout \
#     && ln -sf /app/nginx/logs/error.log /dev/stderr
